;;
;; arch-tag: 2563efd9-5da6-442d-8c98-f4c494b0da16
;;
;; Functions to manage a simple text menu
;;

;; The menu-path contains the current list of our "parent" menus.  The
;; head of this list is where we will return to when we exit the current
;; menu.

(define menu-path '("EXIT MENU"))
(define reset-path
  (lambda ()
    (set! menu-path '("EXIT MENU"))))
(define push-path
  (lambda (name)
    (set! menu-path (cons name menu-path))))
(define pop-path
  (lambda ()
    (set! menu-path (cdr menu-path))))

;;
;; Extract components from a menu object
;;

(define menu-title
  (lambda (menu)
    (car menu)))
(define menu-entries
  (lambda (menu)
    (cdr menu)))

;;
;; Reset the watchdog timer by toggling bit 7 in port E
;;
(define pet-watchdog
  (lambda ()
    (setb 'e #x80)
    (delay-ms 1)
    (clearb 'e #x80)))

;;
;; Read-eval-print loop with a specified timeout
;;

(define timed-repl
  (lambda (tlimit)
    (letrec ((alarm-on
	      (lambda ()
		(if (<= tlimit 0)
		    '()
		    (alarm tlimit 1))))
	     (*stop* 0)
	     (quit
	      (lambda ()
		(*throw 'errobj 1))))
      (while (eqv? *stop* 0)
	     (display "SCHEME>> ")
	     (pet-watchdog)
	     (print (eval (begin
			    (alarm-on)
			    (let ((e (read))) (alarm-off) e))))))))


;;
;; Prompt user for a menu entry and return the corresponding action
;; or nil.
;;

(define select-action
  (lambda (ent)
    (let ((i (get-integer "Menu selection =>" nil)))
      (cond
       ((or (not (number? i)) (> i (length ent))) '())
       ((<= i 0) i)
       ('t (cadr (nth (- i 1) ent)))))))

;;
;; Display a menu and execute the user requests.  The request can result
;; in one of the following actions:
;;
;;    - execution of the code associated with the selected line.
;;    - display of a sub-menu
;;    - exiting the menu
;;    - running a read-eval-print loop (selected by entering a 0)
;;
;; The optional argument 'tlimit' is used to specify a time limit
;; (in seconds) in which the user must enter a selection.
;;

(define show-menu
  (lambda (menu . tlimit)
    (letrec ((T (if (not (null? tlimit))
		    (car tlimit)
		    0))
	     (A
	      (lambda (m)
		(while (not (eq? (B m) 'done)))))
	     (B
	      (lambda (m)
		(let ((title (menu-title m))
		      (entries (menu-entries m)))
		  (display-menu title entries (car menu-path))
		  (and (not (= 0 T)) (alarm T 1))
		  (let ((action-or-menu (select-action entries)))
		    (alarm-off)
		    (pet-watchdog)
		    (cond
		     ((null? action-or-menu) (pop-path) 'done)
		     ((pair? action-or-menu) (push-path title)
					     (A action-or-menu))
		     ((number? action-or-menu) (*catch 'errobj
						       (timed-repl T)))
		     ('t (action-or-menu))))))))
      (reset-path)
      (A menu))))
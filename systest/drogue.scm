;;
;; arch-tag: c5e9bd99-a5d9-4dcf-9839-240bc76b53f7
;;
;; drogue motor test functions
;;

;; Bit mask for the "drogue out" control bit
(define out-mask #x01)

;; Bit mask for the "drogue in" control bit
(define in-mask #x02)

;; Sleep interval in seconds
(define dt-sleep-interval 300)

;; Stop the motor
(define drogue-stop
  (lambda ()
    (clearb 'c (+ in-mask out-mask))))

;; Move the drogue out by setting the "out bit", waiting TWAIT seconds,
;; and then clearing the bit.
(define drogue-out
  (lambda (twait)
    (let ((start (time)))
      (setb 'c out-mask)
      (while (and (not (kbflush)) (<= (- (time) start) twait)))
      (clearb 'c out-mask))))

;; Move the drogue in by setting the "in bit", waiting TWAIT seconds,
;; and then clearing the bit.
(define drogue-in
  (lambda (twait)
    (let ((start (time)))
      (setb 'c in-mask)
      (while (and (not (kbflush)) (<= (- (time) start) twait)))
      (clearb 'c in-mask))))

(define drogue-cycle
  (lambda (n t)
    (let ((i 0))
      (while (< i n)
	     (drogue-out t)
	     (drogue-in t)
	     (set! i (+ i 1))))))

(define drogue-test
  (lambda ()
    (drogue-stop)
    (while (not (kbflush))
	   (drogue-out 30)
	   (if (not (kbflush)) 
	       (sleep dt-sleep-interval))
	   (drogue-in 30)
	   (if (not (kbflush)) 
	       (sleep dt-sleep-interval)))))
;;
;; arch-tag: 14c63bd2-50e6-4057-bf5c-60786c729124
;; Time-stamp: <2019-06-21 21:30:07 mike>
;;
;;

(define logmsg
  (lambda (msg)
    (mapcar display (list (isotime (time)) ": " msg))
    (newline)
    (fflush nil)))

(define motor-stats
  (lambda ()
    (list (motor-pos) (motor-speed)
		(motor-overshoot) (motor-energy))))

(define motor-result
  (lambda (x)
    (let ((msgs '((-1 . "Maximum current exceeded")
		  (-2 . "Motor timed-out")
		  (-3 . "Motor stalled")
		  (-4 . "Motor has not been HOMEd")
		  (-5 . "Keyboard interrupt"))))
      (if (< x 0)
	  (cdr (assoc x msgs))
	  (motor-stats)))))

(define move-motor-fwd
  (lambda ()
    (motor-result (motor-move
		   (from-cm (get-float "How far? (cm)" 5.0)) 1000))))

(define move-motor-rev
  (lambda ()
    (motor-result (motor-move
		   (- 0 (from-cm (get-float "How far? (cm)" 5.0)) 1000)))))

(define bump-motor-fwd
  (lambda ()
    (motor-bump (get-integer "Time interval? (ms)" 2000))))

(define move-motor-home
  (lambda ()
    (let ((p0 (motor-pos))
	  (p1 (motor-home)))
      (if (< p1 0)
	  (motor-result p1)
	  (- p1 p0)))))

(define motor-test
  (lambda (x n)
    (let ((i 0))
      (while (< i n)
	     (motor-home)
	     (while (equal? -2 (begin (logmsg "motor start")
				      (motor-move (- x (motor-pos)) 5)))
		    (logmsg "motor stop"))
	     (set! i (+ i 1))))))

(define motor-tracing-mode 'off)
(define motor-toggle-trace
  (lambda ()
    (if (eq? motor-tracing-mode 'off)
	(begin (motor-trace 1) (set! motor-tracing-mode 'on))
	(begin (motor-trace '()) (set! motor-tracing-mode 'off)))))

(define motor-step
  (lambda (target eps)
    (let ((start (motor-pos)))
      (while (equal? (motor-move (- target (motor-pos)) 60 eps) -2)
	     (delay-ms 1000))
      (motor-pos))))

(define deep-motor-test
  (lambda (x eps)
    (motor-home)
    (let* ((p0 (motor-step (from-cm x) eps))
	   (p1 (motor-home)))
      (display (- p1 p0))
      (newline))
    (let* ((p0 (and (dev-init /dev/gps)
		    (motor-step (from-cm x) eps)))
	   (p1 (and (dev-close /dev/gps)
		    (motor-home))))
      (display (- p1 p0))
      (newline))))

(define update-encoder-params
  (lambda ()
    (let ((type (get-integer "Motor type? (1=standard,2=deep)"))
	  (set-enc (lambda (cpt gr)
		     (nv-insert "motor:cpt" cpt)
		     (nv-insert "motor:gr" gr 1)
		     (motor-set-encoder cpt gr))))
      (if (eq? type 1)
	  (set-enc 100 411)
	  (set-enc 500 546)))))

(define check-encoder-params
  (lambda ()
    (if (not (and (nv-find "motor:cpt") (nv-find "motor:gr")))
	(update-encoder-params)
	(motor-set-encoder (nv-find "motor:cpt")
			   (nv-find "motor:gr")))))

(define xp-range-test
  (lambda ()
    (let ((fout (get-float "Output freq? (khz)" 8))
	  (fin (get-float "Input freq? (khz)" 10))
	  (timeo (get-integer "Timeout? (ms)" 1000))
	  (pw (get-integer "Pulse width? (ms)" 5)))
      (if (or (xp-open) (xp-open))
	  (begin
	    (print "Press any key to stop ...")
	    (while (not (kbflush))
		   (xp-ping (list fout fin) timeo pw 1)
		   (delay-ms (+ timeo 150))
		   (print (xp-range)))
	    (xp-close))))))

(define xp-trans-test
  (lambda ()
    (let ((fout (get-float "Output freq? (khz)" 10))
	  (fin (get-float "Input freq? (khz)" 8))
	  (pw (get-float "Response pulse width? (ms)" 1.5))
	  (timeo (get-integer "Listen time? (ms)" 5000)))
      (if (or (xp-open) (xp-open))
	  (begin
	    (print "Press any key to stop ...")
	    (while (not (kbflush))
		   (xp-listen (list fout fin) pw)
		   (delay-ms timeo)
		   (print (xp-replies)))
	    (xp-close))))))

(define *tracking-menu*
  (list "TRACKING TEST"
	(list "ranging test" xp-range-test)
	(list "transponder test" xp-trans-test)))

(define *motor-menu*
  (list "MOTOR TEST"
	(list (lambda ()
		(if (eq? motor-tracing-mode 'off)
		    "enable position tracing"
		    "disable position tracing"))
	      motor-toggle-trace)
	(list "motor forward" (lambda () (print (move-motor-fwd))))
	(list "motor reverse" (lambda () (print (move-motor-rev))))
	(list "motor home" (lambda () (print (move-motor-home))))
	(list "motor stats" (lambda () (print (motor-stats))))
	(list "update encoder parameters" update-encoder-params)
        (list "bump piston forward" bump-motor-fwd)))

(define drogue-wait-seconds 30)
(define *drogue-menu*
  (list "DROGUE TEST"
	(list "set motor run time" (lambda ()
				     (set! drogue-wait-seconds
					   (get-integer "How long? (secs)"
							drogue-wait-seconds))))
	(list "drogue cycle" (lambda ()
			       (drogue-cycle
				(get-integer "Number of cycles? " 10)
				drogue-wait-seconds)))
	(list "drogue out" (lambda () (drogue-out drogue-wait-seconds)))
	(list "drogue in" (lambda () (drogue-in drogue-wait-seconds)))))

(define dev-display-mode 'continuous)
(define dev-display-scroll 't)

(define update-display
  (lambda (val)
    (display "\r")
    (display val)
    (display "           ")
    (fflush nil)))

(define update-display-scroll
  (lambda (val)
    (display val)
    (newline)
    (fflush nil)))

(define show-dev
  (lambda (dev . arg)
    (let* ((do-update (if (eq? dev-display-scroll 't)
			  update-display-scroll
			  update-display))
	   (opt (if (not (null? arg))
		    (car arg)
		    nil))
	   (D (if (eq? dev-display-mode 'continuous)
		 (lambda ()
		   (print "Press any key to stop ...")
		   (newline)
		   (while (not (kbflush))
			  (pet-watchdog)
			  (do-update (dev-read dev 1 opt)))
		   (newline) 't)
		 (lambda ()
		   (pet-watchdog)
		   (print (dev-read dev 1 opt)) 't))))
      (and (dev-init dev opt) (begin (while (not (dev-ready? dev opt))) 1)
	   (D) (dev-close dev opt)))))

(define run-gtd
  (lambda (mode)
    (dev-init /dev/gtd)
    (print "Press any key to stop ...")
    (delay-ms 3000)
    (while (not (kbflush))
	   (pet-watchdog)
	   (print "Trigger")
	   (start-timer)
	   (dev-config /dev/gtd mode)
	   (print "Waiting for Paros to start...")
	   (while (and (not (kbhit)) (not (dev-ready? /dev/gtd)))
		  (pet-watchdog)
		  )
	   (if (not (kbhit))
	       (begin
		 (print (dev-read /dev/gtd))
		 (print "Waiting for GTD to become ready...")))
	   (while (and (not (kbhit)) (< (read-timer) 40000000)))
	   )
    (dev-close /dev/gtd)))

(define single-read
  (lambda (dev)
    (and (dev-init dev) (begin (while (not (dev-ready? dev))) 1)
	 (let ((r (dev-read dev 1)))
	   (dev-close dev)
	   r))))

(define stress-device
  (lambda (dev)
    (logmsg "Beginning test, press any key to stop")
    (while (not (kbflush))
	   (print (single-read dev)))
    (logmsg "Test complete")))


(define run-ctdo-test
  (lambda ()
    (let* ((t_slow (get-integer "Slow time (seconds)?" 5))
	   (t_fast (get-integer "Fast time (seconds)?" 1)))
      (ctdo-test t_slow t_fast))))

(define camera-test
  (lambda ()
    (let* ((files (camera-filenames))
	   (lowres (car files))
	   (highres (cdr files)))
      (outb 'c (bit-or (inb 'c) #x04))
      (and (camera-open)
	   (begin
	     (delay-ms 1000)
	     (display "Taking low-res snapshot ")
	     (display lowres)
	     (newline)
	     (snapshot res320x240 lowres)
	     (delay-ms 100)
	     (display "Taking high-res snapshot ")
	     (display highres)
	     (newline)
	     (snapshot res640x480 highres)
	     (camera-close)))
      (outb 'c (bit-and (inb 'c) (bit-not #x04))))))

(define *cpuspeed*
  (list "CPU CLOCK SPEED"
	(list "16mhz" (lambda () (setfsys 16000000)))
	(list "8mhz" (lambda () (setfsys 8000000)))
	(list "4mhz" (lambda () (setfsys 4000000)))))

(define *flasher*
  (list "LED Flasher"
	(list (lambda ()
		(if (equal? (bit-and (inb 'c) #x04) #x04)
		    "flasher off"
		    "flasher on"))
	      (lambda ()
		(let ((state (inb 'c)))
		  (outb 'c (bit-xor state #x04)))))))

(define *argos*
  (list "Argos PTT"
	(list (lambda ()
		(if (equal? (bit-and (inb 'c) #x08) #x08)
		    "argos off"
		    "argos on"))
	      (lambda ()
		(if (equal? (bit-and (inb 'c) #x08) #x08)
		    (argos-off)
		    (argos-on)))
	      )))

(define *transponder*
  (list "Acoustic Transponder"
	(list (lambda ()
		(if (equal? (bit-and (inb 'c) #x80) #x80)
		    "transponder off"
		    "transponder on"))
	      (lambda ()
		(let ((state (inb 'c)))
		  (outb 'c (bit-xor state #x80)))))))

(define *anr-state* 'off)
(define *anr-sampling* 'off)
(define *anr*
  (list "Acoustic Noise Recorder"
	(list (lambda ()
		(if (eq? *anr-state* 'off)
		    "power device on"
		    "power device off"))
	      (lambda ()
		(if (eq? *anr-state* 'off)
		    (and (dev-init /dev/anr)
			 (set! *anr-state* 'on))
		    (begin (dev-close /dev/anr)
			   (set! *anr-state* 'off) (set! *anr-sampling* 'off)))))
	(list (lambda ()
		(if (eq? *anr-sampling* 'off)
		    "Start sampling"
		    "Stop sampling"))
	      (lambda ()
		(if (eq? *anr-sampling* 'off)
		    (and
		     (or (dev-config /dev/anr "$sync")
			 (dev-config /dev/anr "$sync"))
		     (or (dev-config /dev/anr "$start")
			 (dev-config /dev/anr "$start"))
		     (set! *anr-sampling* 'on))
		    (begin
		      (and
		       (or (dev-config /dev/anr "$stop")
			   (dev-config /dev/anr "$stop"))
		       (set! *anr-sampling* 'off))))))
	(list "Pass-thru mode" (lambda ()
				 (dev-test /dev/anr)))))

(define *gtd-menu*
  (list "Gas Tension Device"
	(list "Test Pulsed mode" (lambda () (show-dev /dev/gtd 0)))
	(list "Test Continuous mode" (lambda () (show-dev /dev/gtd 1)))
	(list "Pass-through" (lambda ()
			       (dev-init /dev/gtd)
			       (dev-test /dev/gtd)
			       (dev-close /dev/gtd)))))

(define *sbectd-menu*
  (list "SBE41+ CTD"
        (list "First Device" (lambda () (show-dev /dev/sbe41p 0)))
        (list "Second Device" (lambda () (show-dev /dev/sbe41p 1)))))

(define *tchain-0*
  (list "RBR Tchain 0"
        (list "Read Sample" (lambda () (show-dev /dev/tchain 0)))
        (list "Pass-through" (lambda ()
                               (dev-init /dev/tchain 0)
                               (dev-test /dev/tchain 0)
                               (dev-close /dev/tchain 0)))
        (list "Enable Sampling" (lambda ()
                                  (dev-init /dev/tchain 0)
                                  (delay-ms 1000)
                                  (tchain-enable
                                   0
                                   (get-integer "Sample period (sec)?" 3))))
        (list "Disable Sampling" (lambda ()
                                   (tchain-disable 0)
                                   (dev-close /dev/tchain 0)
                                   ))
        (list "Download data" (lambda ()
                                (dev-init /dev/tchain 0)
                                (delay-ms 1000)
                                (tchain-download
                                 0
                                 (get-string "Data file?" "tc0data.rbr"))
                                (dev-close /dev/tchain 0)
                                ))
        ))

(define *tchain-1*
  (list "RBR Tchain 0"
        (list "Read Sample" (lambda () (show-dev /dev/tchain 1)))
        (list "Pass-through" (lambda ()
                               (dev-init /dev/tchain 1)
                               (dev-test /dev/tchain 1)
                               (dev-close /dev/tchain 1)))
        (list "Enable Sampling" (lambda ()
                                  (dev-init /dev/tchain 1)
                                  (delay-ms 1000)
                                  (tchain-enable
                                   1
                                   (get-integer "Sample period (sec)?" 3))))
        (list "Disable Sampling" (lambda ()
                                   (tchain-disable 1)
                                   (dev-close /dev/tchain 1)
                                   ))
        (list "Download data" (lambda ()
                                (dev-init /dev/tchain 1)
                                (delay-ms 1000)
                                (tchain-download
                                 1
                                 (get-string "Data file?" "tc1data.rbr"))
                                (dev-close /dev/tchain 1)
                                ))
        ))

(define *tchain-menu*
  (list "RBR Temperature Chains"
        (list "First Device" *tchain-0*)
        (list "Second Device" *tchain-1*)
        ))

(define *new-pressure-menu*
  (list "3-Channel Pressure"
        (list "Top Pressure" (lambda ()
                               (print "Press any key to stop ...")
                               (newline)
                               (init-new-pressure)
                               (while (not (kbflush))
                                      (pet-watchdog)
                                      (delay-ms 3000)
                                      (fast-pressure 0)
                                      )
                               (close-new-pressure)))
	(list "Hull Pressure" (lambda ()
                            (print "Press any key to stop ...")
                            (newline)
                            (init-new-pressure)
                            (while (not (kbflush))
                                   (pet-watchdog)
                                   (delay-ms 3000)
                                   (fast-pressure 1)
                                   )
                            (close-new-pressure)))
	(list "Bottom Pressure" (lambda ()
                              (print "Press any key to stop ...")
                              (newline)
                              (init-new-pressure)
                              (while (not (kbflush))
                                     (pet-watchdog)
                                     (delay-ms 3000)
                                     (fast-pressure 2)
                                     )
                              (close-new-pressure)))
    ))

(define *sensors*
  (list "SENSORS"
	(list (lambda ()
		(if (eq? dev-display-mode 'continuous)
		    "disable continuous display"
		    "enable continous display"))
	      (lambda ()
		(if (eq? dev-display-mode 'continuous)
		    (set! dev-display-mode 'once)
		    (set! dev-display-mode 'continuous))))
	(list (lambda ()
		(if (eq? dev-display-scroll 't)
		    "disable scrolling display"
		    "enable scrolling display"))
	      (lambda ()
		(if (eq? dev-display-scroll 't)
		    (set! dev-display-scroll nil)
		    (set! dev-display-scroll 't))))
	(list "gps board" (lambda () (show-dev /dev/gps)))
	(list "LiCOR light sensor" (lambda () (show-dev /dev/i490)))
	(list "Humidity sensor" (lambda () (show-dev /dev/rh)))
	(list "Internal pressure" (lambda () (show-dev /dev/ipr)))
        (list "Valeport altimeter" (lambda () (show-dev /dev/alt)))
	(list "Bottom CTD" (lambda () (show-dev /dev/ctd 0)))
	(list "Top CTD" (lambda () (show-dev /dev/ctd 1)))
        (list "SBE41+ CTDs" *sbectd-menu*)
        (list "SBE63 O2 sensor (no pumping)" (lambda () (show-dev /dev/sbe63 0)))
        (list "RBR Temperature Chains" *tchain-menu*)
	(list "Optode" (lambda () (show-dev /dev/optode)))
	(list "FLNTU" (lambda () (show-dev /dev/flntu)))
	(list "CSTAR" (lambda () (show-dev /dev/cstar)))
	(list "3-Channel Pressure" *new-pressure-menu*)
	(list "CTD+Oxygen test" run-ctdo-test)))

(define pressure-cal
  (lambda (chan)
    (if (< chan 0)
        (prog1
         (init-old-pressure)
         (prcal -1)
         (close-old-pressure))
        (prog1
         (init-new-pressure)
         (prcal chan)
         (close-new-pressure)))))

(define *prcal*
  (list "PRESSURE CALIBRATION"
        (list "Top Pressure Sensor"
              (lambda () (pressure-cal 0)))
        (list "Hull Pressure Sensor"
              (lambda () (pressure-cal 1)))
        (list "Bottom Pressure Sensor"
              (lambda () (pressure-cal 2)))
        (list "Single-channel Pressure"
              (lambda () (pressure-cal -1)))))

(define *cfsbc*
  (list "CAMERA FLOAT SBC"
        (list "Pass-thru Test" cfsbc-test)
        (list "Snapshot"
              (lambda()
                (cfsbc-snapshot
                 (get-string "Area of interest? " "")
                 (get-float "Exposure time? (ms) " 175.0)
                 (get-integer "Scale factor? (%) " 50))))))

(define *subtests*
  (list "SUBSYSTEM TESTS"
	(list "Battery voltages" (lambda ()
				   (ad-init)
				   (setb 'd #x20)
				   (delay-ms 150)
				   (print (list (* 4.322 (ad-read 5))
						(* 4.322 (ad-read 6))))
				   (clearb 'd #x20)
				   (ad-close)))
	(list "sensors" *sensors*)
	(list "motor test" *motor-menu*)
	(list "drogue test" *drogue-menu*)
	(list "LED flasher" *flasher*)
        (list "Camera Float SBC" *cfsbc*)
	(list "TriOS radiometers" (lambda ()
				    (trios-test
				     (get-integer "Device (ACC=0, ARC=1)?" 0)
				     (get-integer "Integration time code?" 0)
				     (get-string "Data file?" "trios.sx"))))
	(list "Argos PTT" *argos*)
	(list "Acoustic Noise Recorder" *anr*)
	(list "Acoustic Transponder" *transponder*)))

(define sleep-test
  (lambda ()
    (sleep (get-integer "How long? (secs)" 60))))

(define keep-alive
  (lambda ()
    (print "Sleeping.  Press <CR><CR> to wakeup ...")
    (pet-watchdog)
    (while (not (sleep 30))
	   (pet-watchdog)
	   (print "Sleeping.  Press <CR><CR> to wakeup ..."))))

(define *systests*
  (list "TEST MODE"
	(list "LP sleep" sleep-test)
	(list "Keep alive" keep-alive)
	(list "Activate Explosive Bolt" bolt-test)
	(list "Optode pass-through" (lambda ()
				    (dev-init /dev/optode)
				    (dev-test /dev/optode)
				    (dev-close /dev/optode)))
    (list "Pressure Calibration" *prcal*)
    (list "Iridium pass-through" (lambda()
                                   (irtest (get-integer "Baud rate? " 2400))))
    (list "Sub-system tests" *subtests*)))
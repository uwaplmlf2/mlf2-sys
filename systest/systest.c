/*
** MLF2 system test program.  Provides a menu interface for the user
** to test all of the float subsystems.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "ioports.h"
#include "lpsleep.h"
#include "log.h"
#include "nvram.h"
#include "timer.h"
#include "netcdf.h"
#include "picadc.h"
#include "newpr.h"
#include "oldpr.h"
#include "inertial.h"
#include "siod.h"
#include "lfit.h"
#include "draw.h"
#include "util.h"
#include "iridium.h"
#include "serial.h"
#include "xcat.h"
#include "trios.h"
#include "isus.h"
#include "ctdo.h"
#include "base64.h"
#include "power.h"
#include "tchain.h"
#include "cfsbc.h"
#include "drogue.h"
#include "version.h"
#include "libversion.h"

#define MK_ID() "$Id: " ## SHORT_REV ## " " ## __DATE__ ## " " ## __TIME__ ## " $"

static const char *__id__ = MK_ID();

#define ABS(x)      ((x) < 0 ? -(x) : (x))
#define ARGOS_ON()  iop_set(IO_C, 0x08)
#define ARGOS_OFF() iop_clear(IO_C, 0x08)

void mlf2_qspi_init(void);

static const char *siod_argv[] = {
    "siod",
    "-h8000:1",
    "-s6000",
    "-o400",
    "-n100",
};

static char *scm_cmd = "(begin (*catch 'errobj (require \"menu.scm\"))"
"(*catch 'errobj (require \"testmode.scm\"))"
"(*catch 'errobj (require \"drogue.scm\"))"
"(check-encoder-params)"
"(show-menu *systests*))";

extern char *_mtop, *_mbot, *_mcur;
extern unsigned long    _H0_org;

static long nr_spurious_ints;

static void
spurious_int_handler(void)
{
    nr_spurious_ints++;
}

static LISP
spurious_interrupts(void)
{
    return inumcons(nr_spurious_ints);
}

static int
stroke_watchdog(void)
{
    PET_WATCHDOG();
    return 1;
}

static char *bolt_warning = "WARNING: you are about to test the explosive bolt circuit.
Before proceeding, insure that the bolt is NOT CONNECTED to the float.

The CPU will signal the watchdog. After 5 seconds the watchdog will reset
the CPU, energize the bolt circuit for 30 seconds, and latch the ARGOS relay.
After this test, THE WATCHDOG WILL BE DEACTIVATED. You MUST power-cycle the
float after this test to restart the watchdog. You should also un-latch the
ARGOS relay.

Would you like to continue ";

static LISP
test_bolt(void)
{
    if(!yes_or_no(bolt_warning, 0))
        return NIL;

    EMERGENCY_ABORT();
    return a_true_value();
}

LISP
test_iridium(LISP arg)
{
    snumber_t   phone_sn;
    long        baud;

    baud = NNULLP(arg) ? get_c_long(arg) : 2400L;

    if(iridium_init(baud))
    {
        if(iridium_get_sn(&phone_sn))
            printf("Serial number: %s\n", sn_to_str(phone_sn));

        iridium_passthru();
        iridium_shutdown();
    }

    return a_true_value();
}

static LISP
chat_debug(LISP arg)
{
    serial_chat_debug(get_c_long(arg));
    return a_true_value();
}

static int
query_double(const char *prompt, double *valp)
{
    char line[32];

    fputs(prompt, stdout);
    fflush(stdout);
    if(InputLine(line, (short)sizeof(line)-1) && line[0])
    {
        sscanf(line, "%lf", valp);
        return 1;
    }
    else
        return 0;
}


static double
average_mv_newpr(int chan, int n)
{
    int     i, j, n_read;
    double  v_mean = 0.;
    long    counts[n];

    newpr_clear();
    DelayMilliSecs(1000L);

    printf("Averaging over %d seconds ", n);
    fflush(stdout);
    i = n;
    while(i--)
    {
        DelayMilliSecs(1000L);
        SerPutByte('.');
    }
    SerPutByte('\n');
    n_read = newpr_read_data((pr_channel_t)chan, n, counts);
    if(n_read < n)
        printf("WARNING: only %d values read (expected %d)\n", n_read, n);

    v_mean = 0;
    for(i = 1,j = 0;j < n_read;i++,j++)
        v_mean = v_mean + (newpr_counts_to_mv(counts[j]) - v_mean)/(double)i;
    return v_mean;
}

static double
average_mv_oldpr(int ignored, int n)
{
    int     i, j, n_read;
    double  v_mean = 0.;
    long    counts[n];

    oldpr_clear();
    DelayMilliSecs(1000L);

    printf("Averaging over %d seconds ", n);
    fflush(stdout);
    i = n;
    while(i--)
    {
        DelayMilliSecs(1000L);
        SerPutByte('.');
    }
    SerPutByte('\n');
    n_read = oldpr_read_data(oldpr_get_count(), counts);
    if(n_read < n)
        printf("WARNING: only %d values read (expected %d)\n", n_read, n);

    v_mean = 0;
    for(i = 1,j = 0;j < n_read;i++,j++)
        v_mean = v_mean + (oldpr_counts_to_mv(counts[j]) - v_mean)/(double)i;
    return v_mean;
}


static const char *cal_intro =
"\nThe calibration is performed by entering each reference value reading\n"
"followed by a <CR>.  Entering a <CR> by itself will end the input process.\n"
"A maximum of 10 readings may be entered.  The new linear calibration\n"
"coefficients will be calculated and stored in non-volatile RAM.\n";

/*
 * pr_calibrate - calculate millivolt to PSI calibration constants.
 *
 * Interactive function to calculate the calibration constants to convert
 * from millivolts to PSI.  The constants are stored in NVRAM for later
 * use by pr_mv_to_psi().  Returns the R^2 calibration coefficient.
 */
static double
pr_calibrate(int chan, int (*f_init)(int), void (*f_close)(int),
             double (*f_avg)(int, int),
             const char *c0_name, const char *c1_name)
{
    int     i;
    double  r, c[2], v[10], phys[10];

    if(f_init && !f_init(chan))
    {
        log_error("pr_calibrate", "Cannot access pressure sensor\n");
        return 0.;
    }

    fputs(cal_intro, stdout);
    i = 0;
    while(i < 10)
    {
        if(query_double("ENTER pressure value (psi) => ", &phys[i]))
        {
            if(i > 0 && phys[i] == phys[i-1])
            {
                printf("Replacing previous value\n");
                i--;
            }

            v[i] = f_avg(chan, 10);
            printf("%g, %g\n", phys[i], v[i]);
            i++;
        }
        else
            break;
    }

    if(f_close)
        f_close(chan);

    r = 0;
    if(i >= 2)
    {
        nv_value    nv;

        r = lfit(v, phys, i, c);
        printf("R^2 = %g\n", r);
        printf("Calibration coeff: %g, %g\n", c[0], c[1]);
        nv.d = c[0];
        nv_insert(c0_name, &nv, NV_FLOAT, 0);
        nv.d = c[1];
        nv_insert(c1_name, &nv, NV_FLOAT, 0);
        fputs("Updating NVRAM ... ", stdout);
        fflush(stdout);
        nv_write();
        fputs("done\n", stdout);
    }
    else
        printf("Not enough data points (%d) for calibration\n", i);

    return r;
}

static LISP
pr_cal(LISP index)
{
    int     i = get_c_long(index);
    LISP    rval;

    if(i < 0 || i > 2)
        oldpr_spi_init();
    else
        newpr_spi_init();

    DelayMilliSecs(2000);

    if(i == 0)
        rval = flocons(pr_calibrate((1 << i), NULL, NULL,
                                    average_mv_newpr, "PIC0:psi-offset",
                                    "PIC0:psi/mv"));
    else if(i == 1)
        rval = flocons(pr_calibrate((1 << i), NULL, NULL,
                                    average_mv_newpr, "PIC1:psi-offset",
                                    "PIC1:psi/mv"));
    else if(i == 2)
        rval = flocons(pr_calibrate((1 << i), NULL, NULL,
                                    average_mv_newpr, "PIC2:psi-offset",
                                    "PIC2:psi/mv"));
    else
        rval = flocons(pr_calibrate(0, NULL, NULL,
                                    average_mv_oldpr, "PIC0:psi-offset",
                                    "PIC0:psi/mv"));

    return rval;
}

static LISP
init_new_pressure(void)
{
    newpr_init();
    return a_true_value();
}

static LISP
close_new_pressure(void)
{
    newpr_shutdown();
    return a_true_value();
}

static LISP
init_old_pressure(void)
{
    oldpr_init(250L);
    return a_true_value();
}

static LISP
close_old_pressure(void)
{
    oldpr_shutdown();
    return a_true_value();
}

static LISP
read_new_pressure(int chan)
{
    int     n, n0, i;
    double  dbars, psi;
    long    p[64];

    n0 = newpr_get_count();
    n = newpr_read_data((pr_channel_t)chan, 64, p);
    if(ABS(n-n0) > 1 || n <= 0)
    {
        newpr_adreset();
        DelayMilliSecs(2000);
        newpr_clear();
        return NIL;
    }

    /* Show every value in the buffer */
    for(i = 0;i < n;i++)
    {
        psi = newpr_mv_to_psi((pr_channel_t)chan, newpr_counts_to_mv(p[i]));
        dbars = psi/1.47;
        printf("psi:%.3f, dbars:%.3f\n", psi, dbars);
    }

    return a_true_value();
}

static LISP
read_old_pressure(void)
{
    int     n, i;
    double  dbars, psi;
    long    p[64];

    n = oldpr_read_data(oldpr_get_count(), p);

    /* Show every value in the buffer */
    for(i = 0;i < n;i++)
    {
        psi = oldpr_mv_to_psi(oldpr_counts_to_mv(p[i]));
        dbars = psi/1.47;
        printf("psi:%.3f, dbars:%.3f\n", psi, dbars);
    }

    return a_true_value();
}

static LISP
read_fast_pressure(LISP index)
{
    int     which;

    which = NNULLP(index) ? get_c_long(index) : 0;

    return which < 0 ? read_old_pressure() : read_new_pressure((int)(1 << which));
}

static LISP
argos_on(void)
{
    char  pbuf[32];  /* random data for packet */

    ARGOS_ON();
    return xcat_setup(pbuf, 31) ? a_true_value() : NIL;
}

static LISP
argos_off(void)
{
    ARGOS_OFF();
    return a_true_value();
}

static int
store_raw_frame(DATAFRAME_t *dfp, void *cbdata)
{
    FILE        *ofp = (FILE*)cbdata;

    fprintf(ofp, "(frame #%02x# #%x# %d:",
            dfp->module, dfp->index, dfp->size);
    fwrite(dfp->data, 1L, dfp->size, ofp);
    fputs(")\n", ofp);

    return 1;
}

static int
store_frame(DATAFRAME_t *dfp, void *cbdata)
{
    FILE        *ofp = (FILE*)cbdata;
    unsigned char   *p;
    int         i, rem, len;

    fprintf(ofp, "(frame #%02x# #%x# |",
            dfp->module, dfp->index);
    for(i = 0,p = dfp->data;i < dfp->size;i += 3,p += 3)
    {
        rem = dfp->size - i;
        len = (rem < 3) ? rem : 3;
        b64_write(ofp, p, len);
    }
    fputs("|)", ofp);

    return 1;
}

static LISP
test_trios(LISP idx, LISP tcode, LISP filename, LISP useraw)
{
    int         which, timecode, count, n;
    FILE        *ofp;
    char        *tag;
    unsigned long   t;
    trios_datasink  handler;

    which = get_c_long(idx);

    if((ofp = fopen(get_c_string(filename), "a")) == NULL)
    {
        log_error("trios", "Cannot open output file\n");
        return NIL;
    }

    if(!trios_init(which))
    {
        log_error("trios", "Cannot access device\n");
        fclose(ofp);
        return NIL;
    }

    while(!trios_dev_ready(which))
        ;

    if(which == 0)
    {
        log_event("Running initial dummy sample\n");
        trios_set_inttime(0, 1);
        DelayMilliSecs(10L);
        trios_start(0);
        while(!trios_data_ready(which))
            ;
        trios_read_data(0, 11, 2000L, NULL, NULL);
        count = 9;
        tag = "trios-acc";
    }
    else
    {
        tag = "trios-arc";
        count = 8;
    }

    if(NNULLP(useraw))
        handler = store_raw_frame;
    else
        handler = store_frame;

    trios_set_inttime(which, timecode = get_c_long(tcode));
    printf("Press any key to stop ...\n");

    do
    {
        log_event("Sampling spectra\n");
        t = RtcToCtm();
        trios_start(which);
        fprintf(ofp, "(%s #%lx# ", tag, t);
        while(!trios_data_ready(which))
            ;
        n = trios_read_data(which, count, 3000L, handler, (void*)ofp);
        fputs(")\n", ofp);
        log_event("Read %d data frames\n", n);
        if(count == 11)
            count = 9;

    } while(!SerByteAvail());
    (void)SerGetByte();

    trios_shutdown(which);
    fclose(ofp);

    return a_true_value();
}

static LISP
tchain_download_data(LISP idx, LISP filename)
{
    if(tchain_download((int)get_c_long(idx), 1, get_c_string(filename)) == 0)
        return NIL;
    return a_true_value();
}

static LISP
tchain_enable_sampling(LISP idx, LISP period)
{
    if(tchain_enable((int)get_c_long(idx), get_c_long(period)*1000) == 0)
        return NIL;
    return a_true_value();
}

static LISP
tchain_disable_sampling(LISP idx)
{
    return tchain_disable((int)get_c_long(idx)) ? a_true_value() : NIL;
}

static void
store_isus_data(FILE *ofp, unsigned long t, IsusData *idp, int conc)
{
    const char      *tag;
    unsigned char   *p;
    int         i, rem, len, n;

    if(conc)
    {
        tag = "isus-conc";
        n = ISUS_CONC_FRAME;
    }
    else
    {
        tag = "isus-full";
        n = ISUS_FULL_FRAME;
    }

    fprintf(ofp, "(%s #%lx# |", tag, t);
    p = idp->dark;
    for(i = 0;i < n;i += 3,p += 3)
    {
        rem = n - i;
        len = (rem < 3) ? rem : 3;
        b64_write(ofp, p, len);
    }
    fputs("|)\n", ofp);

    fprintf(ofp, "(%s #%lx# |", tag, t);
    p = idp->light;
    for(i = 0;i < n;i += 3,p += 3)
    {
        rem = n - i;
        len = (rem < 3) ? rem : 3;
        b64_write(ofp, p, len);
    }
    fputs("|)\n", ofp);

}

static LISP
test_isus(LISP filename, LISP conc_only)
{
    FILE        *ofp;
    unsigned long   t;
    int         n;
    static IsusData id;

    if((ofp = fopen(get_c_string(filename), "a")) == NULL)
    {
        log_error("isus", "Cannot open output file\n");
        return NIL;
    }

    if(!isus_dev_ready())
    {
        log_event("Waiting for ISUS to start up\n");

        while(!isus_dev_ready())
            ;
    }

    t = RtcToCtm();

    if(!isus_start())
    {
        log_error("isus", "Cannot start sampling process\n");
        fclose(ofp);
        return NIL;
    }

    log_event("Waiting for data sample\n");
    while(!isus_data_ready())
        ;
    n = isus_read_data(&id, 4000L);
    if(n > 0)
    {
        log_event("Storing ISUS data\n");
        store_isus_data(ofp, t, &id, NNULLP(conc_only));
    }

    fclose(ofp);

    return a_true_value();
}

static LISP
test_ctdo(LISP slow, LISP fast, LISP pr)
{
    CTDOdata    ctdo;
    long    t;
    double  p;

    if(!ctdo_init())
        return NIL;

    printf("Press any key to stop ...\n");

    do
    {
        if(!(ctdo_start_oxy() || ctdo_start_oxy()))
            log_error("ctdo", "Cannot start O2 sensor\n");

        t = RtcToCtm();
        log_event("Pumping slow\n");
        ctdo_cmd("pumpslow\r\n");
        while((RtcToCtm() - t) < get_c_long(slow))
            ;
        t = RtcToCtm();
        log_event("Pumping fast\n");
        ctdo_cmd("pumpfast\r\n");
        while((RtcToCtm() - t) < get_c_long(fast))
            ;

        p = NNULLP(pr) ? get_c_double(pr) : 0.;

        log_event("Starting sample\n");

        ctdo_start_sample(p);
        while(!ctdo_data_ready())
            ;
        if(ctdo_read_data(&ctdo))
        {
            printf("(%.3f, %.3f, %ld)\n", ctdo.t, ctdo.s, ctdo.oxy);
        }
        ctdo_cmd("pumpoff\r\n");
        ctdo_stop_oxy();
    } while(!SerByteAvail());
    (void)SerGetByte();

    ctdo_shutdown();

    return a_true_value();
}

static LISP
test_cfsbc(void)
{
    long        tlimit;

    if(!cfsbc_init())
        return NIL;

    printf("SBC booting, stand by ...\n");
    cfsbc_test();
    printf("SBC shutting down ... ");
    fflush(stdout);
    cfsbc_sleep(3600L);

    // Wait for clean shutdown
    tlimit = RtcToCtm() + 10L;
    do
    {
        if(cfsbc_done())
            break;
        DelayMilliSecs(100L);
    } while(tlimit > RtcToCtm());
    printf("done\n");
    cfsbc_shutdown();
    return NIL;
}

#define OPEN_SHUTTER()          drogue_start_open(5L, 0)
#define CLOSE_SHUTTER()         drogue_start_close(5L, 0)
#define SHUTTER_WAIT()          drogue_wait()
#define LIGHT_ON()              iop_set(IO_C, 0x04)
#define LIGHT_OFF()             iop_clear(IO_C, 0x04)

static void
disable_light(void)
{
    LIGHT_OFF();
}

/*
 * Take a single photo
 */
static int
snapshot(char *photo_aoi, double exposure, long photo_scale)
{
    long        tlimit;
    int         ready, r;
    FILE        *ofp;
    char        *filenames[2];
    char        buf[2][16];

    if(!cfsbc_init())
        return 0;

    printf("SBC booting ... ");
    fflush(stdout);
    OPEN_SHUTTER();
    ready = 0;
    tlimit = RtcToCtm() + 20L;
    SHUTTER_WAIT();
    do
    {
        if(cfsbc_dev_ready())
        {
            ready = 1;
            break;
        }
        DelayMilliSecs(500L);
    } while(tlimit > RtcToCtm());

    if(!ready)
    {
        printf("failed!\n");
        cfsbc_shutdown();
        return 0;
    }
    printf("done\n");

    cfsbc_set_clock();
    LIGHT_ON();
    cfsbc_shell_command("clean outbox\n", 10000L);

    printf("Taking photo and downloading files ...  ");
    fflush(stdout);

    filenames[0] = &buf[0][0];
    filenames[1] = &buf[1][0];
    r = cfsbc_oneshot(photo_aoi, exposure, (unsigned)photo_scale, 60000L,
                      filenames, 15L, disable_light);
    if(r)
        printf("done (filenames = %s, %s)\n", filenames[0], filenames[1]);
    else
        printf("failed!\n");

    LIGHT_OFF();
    cfsbc_shell_command("log\n", 5000L);

    printf("Shutting down SBC ... ");
    fflush(stdout);

    CLOSE_SHUTTER();
    cfsbc_sleep(3600L);
    SHUTTER_WAIT();

    if((ofp = fopen("cfsbclog.txt", "wb")) != NULL)
    {
        fputs(cfsbc_shell_response(0L), ofp);
        fclose(ofp);
    }

    // Wait for clean shutdown
    tlimit = RtcToCtm() + 10L;
    do
    {
        if(cfsbc_done())
            break;
        DelayMilliSecs(100L);
    } while(tlimit > RtcToCtm());
    cfsbc_shutdown();
    printf("done\n");

    return r;
}

static LISP
single_photo(LISP aoi, LISP exposure, LISP scale)
{
    long        t0 = MilliSecs();
    snapshot(get_c_string(aoi), get_c_double(exposure), get_c_long(scale));
    printf("Elapsed time: %ld ms\n", MilliSecs()-t0);

    return NIL;
}

void
mlf2_qspi_init(void)
{
    unsigned   cs_mask, cntl_mask;

    cs_mask   = M_PCS0 | M_PCS1 | M_PCS2 | M_PCS3;
    cntl_mask = M_SCK | M_MOSI | M_MISO;

    /*
    ** Configure the QSPI control and chip select lines as
    ** general purpose outputs. Force all lines except CS3
    ** low. CS3 is the chip-select for the MAX186 and is
    ** active low.
    */
    *QPAR = 0;
    *QPDR = M_PCS3;
    *QDDR = (cs_mask | cntl_mask);
}

int
main(void)
{
    int         timer_id;
    static ExcCFrame    eframe;

    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
        printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
        Reset();

    SimSetFSys(16000000L);
    mlf2_qspi_init();

    /* Catch spurious interrupts */
    InstallHandler(spurious_int_handler, Spurious_Interrupt, &eframe);


    printf("\nMLF2 System Test Program (%s %s)\n", __DATE__, __TIME__);
    printf("Revision: %s\n\n", SHORT_REV);

    init_ioports();
    init_nvram();

    openlog("testlog.txt");
    log_event("Software revision: %s\n", SHORT_REV);
    log_event("Library revision: %s\n", MLF2LIB_REVISION);

    init_sleep_hooks();
    add_after_hook("QSPI-init", mlf2_qspi_init);
    add_before_hook("timer-stop", suspend_timers);
    add_after_hook("timer_start", resume_timers);

    init_cpu_clock();

    process_cla((int)(sizeof(siod_argv)/sizeof(char*)), siod_argv, 1);
    print_hs_1();
    init_storage();
    init_subrs();
    init_trace();

    init_tt8_lib(0);
    init_mlf2_lib(0);
    init_subr_0("spurious-interrupts", spurious_interrupts);
    init_subr_1("prcal", pr_cal);
    init_subr_1("irtest", test_iridium);
    init_subr_1("fast-pressure", read_fast_pressure);
    init_subr_0("init-old-pressure", init_old_pressure);
    init_subr_0("init-new-pressure", init_new_pressure);
    init_subr_0("close-old-pressure", close_old_pressure);
    init_subr_0("close-new-pressure", close_new_pressure);
    init_subr_1("debug-chat", chat_debug);
    init_subr_0("argos-on", argos_on);
    init_subr_0("argos-off", argos_off);
    init_subr_4("trios-test", test_trios);
    init_subr_2("isus-test", test_isus);
    init_subr_3("ctdo-test", test_ctdo);
    init_subr_0("bolt-test", test_bolt);
    init_subr_0("cfsbc-test", test_cfsbc);
    init_subr_3("cfsbc-snapshot", single_photo);
    init_subr_2("tchain-download", tchain_download_data);
    init_subr_2("tchain-enable", tchain_enable_sampling);
    init_subr_1("tchain-disable", tchain_disable_sampling);

    /*
    ** Initialize the timer and setup a timer handler to be called
    ** every 60 seconds to stroke the watchdog.
    */
    timer_id = set_timer(60L, stroke_watchdog);

    power_on(QSPIDEV_POWER1);
    repl_c_string(scm_cmd, 0L, 0L, 0L);
    power_off(QSPIDEV_POWER1);

    log_event("Caught %ld spurious interrupts\n", nr_spurious_ints);

    unset_timer(timer_id);
    shutdown_timer();
    closelog();

    return 0;
}

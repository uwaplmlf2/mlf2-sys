/*
** arch-tag: 7f6ba82a-8a6f-4799-a0c9-621c78f8bc41
**
** I/O Test program for the VM/MLF2 board.
**
**
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <qsm332.h>
#include <picodcf8.h>
#include <userio.h>
#include "menu.h"
#include "ioports.h"
#include "tpuart.h"
#include "lpsleep.h"
#include "power.h"
#include "atod.h"

extern unsigned long 	_H0_org;
static char line[80];

#define FILTER_CODE(x)	(unsigned)(19200/(x))
#define CHOICES(v)	(sizeof(v)/sizeof(int))


static int
yes_or_no(const char *prompt)
{
    int		c;
    
    while(SerByteAvail())
	(void)SerGetByte();
    
    printf("\n%s (y or n)? [n] ", prompt);
    fflush(stdout);
    c = getchar();

    while(c != '\n' && getchar() != '\n')
	;
    
    return (c == 'Y' || c == 'y') ? 1 : 0;
}


/*
 * tpuart_test - menu callback to test TPU Uart functionality.
 */
void
tpuart_test(void *call_data)
{
    TPUart	*t;
    int		n, rchan, wchan, parity_code, bits;
    long	speed;
    char	parity;
    
    printf("*** ENTER parameters (rchan,wchan,baudrate[,parity]) => ");
    fflush(stdout);
    if(!InputLine(line, (short)sizeof(line)-1))
	return;
    n = sscanf(line, "%d,%d,%ld,%1c", &rchan, &wchan, &speed, &parity);
    if(n < 3)
    {
	printf("INVALID input: %s\n", line);
	return;
    }
    
    if(n == 3)
	parity = 'n';

    switch(parity)
    {
	case 'E':
	case 'e':
	    parity_code = TPU_EVEN_PARITY;
	    bits = 7;
	    printf("\nUsing even parity\n");
	    break;
	case 'O':
	case 'o':
	    parity_code = TPU_ODD_PARITY;
	    printf("\nUsing odd parity\n");
	    bits = 7;
	    break;
	default:
	    parity_code = TPU_NO_PARITY;
	    bits = 8;
	    break;
    }
    
    if((t = tpuart_open(rchan, wchan, speed, parity_code, bits, 1, 0)) == 0)
    {
	printf("Cannot open TPU Uart (%d, %d, %ld)\n", rchan, wchan, speed);
	return;
    }
    
    printf("\n--- Pass-through mode to TPU Uart.  Type Ctrl-c to exit\n\n");
    tpu_pass_thru(t, 0x03, 0x02, NULL);
    tpuart_close(t);
}

static void
show_iop_commands(void)
{
    printf("--- Commands: S[et]   <port><line>      (e.g. S b2) \n");
    printf("              C[lear] <port><line>      (e.g. C a3)\n");
    printf("              R[ead]  <port>\n");
    printf("              W[rite] <port> <binary#>  (e.g. W c 00100101)\n");
    printf("              Q   quit\n");
    printf("              ?   display this help message\n");
    printf("    Commands and port ids are case-insensitive\n");
    printf("    Set, Clear, and Read also work with TPU lines, use T for <port>\n");
    printf("    and 0-15 for <line>, otherwise the following rules apply\n\n");
    
    printf("      Valid <ports> are a - f (or A - F)\n");
    printf("      Valid <lines> are 0 - 7\n");
    printf("      For writes, all 8 bits must be specified\n");

}

/*
** Convert a letter a - f (or A - F) to a port id number.
*/
static int
get_port_id(char p)
{
    return (IO_A + (tolower(p) - 'a'));
}

/*
** Print the binary representation of an 8-bit value to stdout.
*/
static void
print_bits(unsigned char x)
{
    register int	bit, i;
    
    for(bit = 0x80,i = 8;i > 0;i--,bit >>= 1)
	putchar((x & bit) ? '1' : '0');
    putchar('\n');
}

/*
** Read the binary representation of an N-bit binary value (N <= 16)
** from a string.
*/
static int
read_bits(register char *str, int n)
{
    register int	i, bit;
    int			value;
    
    value = 0;
    for(bit = 1 << (n-1),i = n;*str && i > 0;i--,bit >>= 1)
	if(*str++ == '1')
	    value |= bit;
    return value;
}

/*
 * ioport_test - menu callback to manipulate I/O port bits.
 */
void
ioport_test(void *call_data)
{
    register char	*ptr;
    int			port, pin;
    unsigned char	value;
    
    show_iop_commands();
    while(1)
    {
	printf("IOP> ");
	fflush(stdout);
	if(!InputLine(line, (short)sizeof(line)-1))
	    continue;

	/* Check for quit */
	if(line[0] == 'q' || line[0] == 'Q')
	    break;

	/* Eat leading white space */
	for(ptr = line+1;*ptr == ' ';ptr++)
	    ;

	/* Check for help request or missing argument(s) */
	if(line[0] == '?' || *ptr == 0)
	{
	    show_iop_commands();
	    continue;
	}

	/*
	** Evaluate commands
	*/
	switch(line[0])
	{
	    case 's':		/* Set line */
	    case 'S':
		if((ptr[0] == 't' || ptr[0] == 'T') && isdigit(ptr[1]))
		{
		    pin = (ptr[1] - '0');
		    if(isdigit(ptr[2]))
			pin = (pin*10) + (ptr[2] - '0');
		    TPUSetPin(pin, 1);
		}
		else
		{
		    port = get_port_id(ptr[0]);
		    value = iop_read(port);
		    value |= (1 << (ptr[1] - '0'));
		    iop_write(port, value);
		    print_bits(iop_read(port));
		}
		break;
	    case 'c':		/* Clear line */
	    case 'C':
		if((ptr[0] == 't' || ptr[0] == 'T') && isdigit(ptr[1]))
		{
		    pin = (ptr[1] - '0');
		    if(isdigit(ptr[2]))
			pin = (pin*10) + (ptr[2] - '0');
		    TPUSetPin(pin, 0);
		}
		else
		{
		    port = get_port_id(ptr[0]);
		    value = iop_read(port);
		    value &= ~(1 << (ptr[1] - '0'));
		    iop_write(port, value);
		    print_bits(iop_read(port));
		}
		break;
	    case 'r':		/* Read port (all lines) */
	    case 'R':
		if((ptr[0] == 't' || ptr[0] == 'T'))
		{
		    for(pin = 15;pin >=0;pin-=4)
		    {
			printf("%d%d%d%d ", TPUGetPin(pin), TPUGetPin(pin-1),
			       TPUGetPin(pin-2), TPUGetPin(pin-3));
		    }
		    printf("\n");
		}
		else
		{
		    
		    port = get_port_id(ptr[0]);
		    /*
		    ** For portA, we need to read the physical port
		    ** rather than the virtual port.
		    */
		    if(port == IO_A)
			port = IO_A_REAL;
		    print_bits(iop_read(port));
		}
		break;
	    case 'w':		/* Write port (all lines) */
	    case 'W':
		port = get_port_id(ptr[0]);
		ptr++;
		while(*ptr == ' ')
		    ptr++;
		value = read_bits(ptr, 8);
		iop_write(port, value);
		print_bits(iop_read(port));
		break;
	    default:
		show_iop_commands();
		break;
	}
    }
    
}

/*
 * sleep_test - menu callback to test LP sleep functions.
 */
void
sleep_test(void *call_data)
{
    int			r;
    unsigned long	secs = 60L;

    if(QueryNum("\nSleep time (seconds)", "%ld", "%ld", &secs))
    {
        printf("\nSleeping for %ld seconds ... ", secs);
        fflush(stdout);

	r = isleep(secs);

        printf("done (%d)\n", r);
    }
}

void
sleep_cycle(void *call_data)
{
    unsigned long	on = 10L, off = 10L, n = 10L;
    register long	i;
    
    printf("\nEnter cycle desc. (sleep-time,wake-time,#cycles) [%ld,%ld,%ld]: ",
	   on, off, n);
    fflush(stdout);
    if(!InputLine(line, (short)sizeof(line)-1))
	return;
    if(sscanf(line, "%ld,%ld,%ld", &on, &off, &n) != 3)
    {
	printf("INVALID input: %s\n", line);
	return;
    }

    /* Convert to milliseconds */
    off *= 1000L;
    
    for(i = 0;i < n;i++)
    {
	printf("\rsleeping %ld    ", i);
	fflush(stdout);
	if(isleep(on) == 1)
	    break;
	printf("\rawake %ld       ", i);
	fflush(stdout);
	DelayMilliSecs(off);
    }
    
    printf("\ndone\n");
}




static unsigned char	_qpar, _qddr, _qpdr;

static void
fix_qspi(void)
{
    _qpar = *QPAR;
    _qddr = *QDDR;
    _qpdr = *QPDR;

    /*
    ** Make PCS1 a general purpose output line and set it high
    ** to prevent activating the external A/D converter.
    */
    _QPDR->PCS1 = SET;
    _QDDR->PCS1 = OUTP;
    _QPAR->PCS1 = CLR;

    DelayMilliSecs(1L);    
}

static void
restore_qspi(void)
{
    *QPDR = _qpdr;
    *QDDR = _qddr;
    *QPAR = _qpar;
}


static void
internal_ad_test(void *call_data)
{
    unsigned long	x, x_min, x_max;
    double		x_mean;
    long		value, nr_samples, i;
    unsigned		chan;

    value = 0;
    if(!QueryNum("\nSelect channel 0-15 => ", "%ld", "%ld", &value) ||
	value < 0 || value > 15)
	return;

    chan = value;
    atod_init();

    nr_samples = 10;
    
 loop:
    if(QueryNum("\nHow many samples ", "%ld", "%ld", &nr_samples))
    {
	x_max = 0;
	x_min = (1L << 24);
	x_mean = 0;
	printf("Sampling channel %d: ", chan);
	fflush(stdout);
	
	for(i = 0;i < nr_samples;i++)
	{
	    x = atod_read(chan);
	    x_mean = x_mean + (x - x_mean)/(double)(i + 1);
	    if(x > x_max)
		x_max = x;
	    if(x < x_min)
		x_min = x;
	    printf("%8ld\n", x);
	    
	}

	fputs(" done\n\n", stdout);

	fputs(" #samples      mean      min      max\n", stdout);
	fputs("======================================\n", stdout);    

	printf(" %6ld    %8.1f  %8ld    %8ld\n\n", nr_samples, x_mean,
	       x_min, x_max);
    
	if(yes_or_no("Go again"))
	    goto loop;
    }

    atod_shutdown();
    
}

static MenuEntry testentries[] = {
{ "Test TPU Uarts",	MENTRY_FUNC,	tpuart_test,	(void*)0 },
{ "Test I/O Ports",	MENTRY_FUNC,	ioport_test,	(void*)0 },
{ "LP Sleep",		MENTRY_FUNC,	sleep_test,	(void*)0 },
{ "LP Sleep Cycle",	MENTRY_FUNC,	sleep_cycle,	(void*)0 },
{ "Internal A/D Test",	MENTRY_FUNC,	internal_ad_test,	(void*)0 },
};

static Menu testmenu = { "OPERATIONS", 0, 0, testentries };


int
main(void)
{
    time_t	secs;
    struct tm	*now;
    
    /*
    ** Hardware initialization
    */
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	Reset();

    SimSetFSys(16000000L);
    
    printf("\n\nMLF2/VMG I/O Test Program\n");
    printf("$Revision: 58c228d0a9ff $  (%s %s)\n\n\n", __DATE__, __TIME__);

    /*
    ** Initialize the clock
    */
    secs = RtcToCtm();
    now = localtime(&secs);
    SetTimeTM(now, NULL);

    init_ioports();
    init_sleep_hooks();
    add_before_hook("IOP-clear", iop_clear_all);

    /*
    ** Display test menu
    */
    testmenu.nr_entries = (int)(sizeof(testentries)/sizeof(MenuEntry));
    show_menu(&testmenu, NULL);
    

    /*
    ** If we are running from flash, exit to the monitor.
    */
    if(((long)&_H0_org) == 0x2000L)
	ResetToMon();

    return 0;
}


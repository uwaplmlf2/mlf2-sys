cmake_minimum_required(VERSION 3.13)

find_package(libtt8 REQUIRED)
find_package(libmlf2 REQUIRED)

set(CMAKE_EXECUTABLE_SUFFIX ".coff")
add_executable(iotest iotest.c)
target_link_libraries(iotest PRIVATE
  ${LIBMLF2_LIBRARY}
  ${LIBTT8_LIBRARY})
target_link_options(iotest BEFORE PRIVATE -T${LINKERFILE_RAM})
target_include_directories(iotest PRIVATE
  ${LIBTT8_INCLUDE_DIR}
  ${LIBMLF2_INCLUDE_DIR})

add_custom_command(TARGET iotest
  COMMAND ${CMAKE_OBJCOPY} -O binary -S $<TARGET_FILE:iotest>
  ${CMAKE_CURRENT_BINARY_DIR}/$<TARGET_NAME:iotest>.run)

install(PROGRAMS
  ${CMAKE_CURRENT_BINARY_DIR}/$<TARGET_NAME:iotest>.run
  DESTINATION "${PKG_DIR}")

find_library(
  LIBSIOD_LIBRARY
  NAMES libsiod.a
  HINTS /usr/local/mlf2/lib ${PROJECT_BINARY_DIR}/prebuilt/mlf2/lib/ ENV MLF2LIBDIR
  NO_DEFAULT_PATH)

find_path(
  LIBSIOD_INCLUDE_DIR
  NAMES siod.h siodp.h
  HINTS /usr/local/mlf2/include ${PROJECT_BINARY_DIR}/prebuilt/mlf2/include/ ENV MLF2INCLUDE)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libsiod DEFAULT_MSG
  LIBSIOD_LIBRARY
  LIBSIOD_INCLUDE_DIR)

mark_as_advanced(
  LIBSIOD_LIBRARY
  LIBSIOD_INCLUDE_DIR)

if(LIBSIOD_FOUND AND NOT TARGET libsiod::libsiod)
  add_library(libsiod::libsiod STATIC IMPORTED)
  set_target_properties(
    libsiod::libsiod
    PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${LIBSIOD_INCLUDE_DIR}"
    IMPORTED_LOCATION "${LIBSIOD_LIBRARY}")
endif()

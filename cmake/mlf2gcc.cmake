set(CMAKE_SYSTEM_NAME               Generic)
set(CMAKE_SYSTEM_PROCESSOR          m68k)

set(MLF2_SYS_LIBDIR /usr/local/m68k-coff/lib/mcpu32)
set(LINKERFILE_FLASH "${MLF2_SYS_LIBDIR}/tt8-flash.ld")
set(LINKERFILE_RAM "${MLF2_SYS_LIBDIR}/tt8-ram.ld")

# Without that flag CMake is not able to pass test compilation check
set(CMAKE_TRY_COMPILE_TARGET_TYPE   STATIC_LIBRARY)

set(CMAKE_AR                        ${TOOLCHAIN_PATH}m68k-coff-ar)
set(CMAKE_ASM_COMPILER              ${TOOLCHAIN_PATH}m68k-coff-gcc)
set(CMAKE_C_COMPILER                ${TOOLCHAIN_PATH}m68k-coff-gcc)
set(CMAKE_CXX_COMPILER              ${TOOLCHAIN_PATH}m68k-coff-g++)
set(CMAKE_LINKER                    ${TOOLCHAIN_PATH}m68k-coff-ld)
set(CMAKE_OBJCOPY                   ${TOOLCHAIN_PATH}m68k-coff-objcopy CACHE INTERNAL "")
set(CMAKE_RANLIB                    ${TOOLCHAIN_PATH}m68k-coff-ranlib CACHE INTERNAL "")
set(CMAKE_SIZE                      ${TOOLCHAIN_PATH}m68k-coff-size CACHE INTERNAL "")
set(CMAKE_STRIP                     ${TOOLCHAIN_PATH}m68k-coff-strip CACHE INTERNAL "")

set(CMAKE_C_FLAGS                   "${APP_C_FLAGS} -Os -mcpu32" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS                 "${APP_CXX_FLAGS} ${CMAKE_C_FLAGS} -fno-exceptions" CACHE INTERNAL "")

set(CMAKE_C_FLAGS_DEBUG             "-Os -g" CACHE INTERNAL "")
set(CMAKE_C_FLAGS_RELEASE           "-Os -DNDEBUG" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_DEBUG           "${CMAKE_C_FLAGS_DEBUG}" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_RELEASE         "${CMAKE_C_FLAGS_RELEASE}" CACHE INTERNAL "")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

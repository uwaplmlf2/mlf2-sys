#
# arch-tag: common makefile components for TT8 code (DOS version)
#
ifndef tt8cross
tt8cross := 1

AR = m68k-coff-ar
CCOPTS  = -D__TT8__ -ps -mc -md -qq -wl -wa -wo -sa
CC = c68 $(CCOPTS)
LD = ln68
AS = as68
OBJCOPY = 
RANLIB = 


#
# Memory layout parameters, 1Mbyte of RAM is assumed by default.  Run make 
# with the SMALLMEM variable defined for the 256kbyte boards.
#
ROMBASE = 2000
ifdef SMALLMEM
RAMBASE   = 2c2000
STACKSIZE = 2000
else
RAMBASE   = 204000
STACKSIZE = 4000
endif

#
# RAM/ROM (flash) loading.  The default is to link the image for loading
# into ROM.  Override bye defining the USERAM variable.
#
ifdef USERAM
BIN = run
HEX = rhx
LDFLAGS = +q -t -m +c $(RAMBASE) +j $(RAMBASE) +s $(STACKSIZE)
SRFLAGS = -p4000 -a3 -b$(RAMBASE)
else
BIN = app
HEX = ahx
LDFLAGS = +q -t -m +c $(ROMBASE) +j $(RAMBASE) +d $(RAMBASE) +s $(STACKSIZE)
SRFLAGS = -p4000 -a3 -b$(ROMBASE)
endif

# Onset libraries
SYSLIBS = -ltt8 -lml16tt8 -lcl16tt8 -ltt8


COMPILE = $(CC) $(DEFS) $(INCLUDES) $(CPPFLAGS) $(CFLAGS)
LINK = $(LD) -o $*.$(BIN) $(LDFLAGS) -f $(LINKFILE) $(SYSLIBS)

endif
